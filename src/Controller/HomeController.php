<?php

namespace App\Controller;

use App\Entity\Post;
use App\Form\PostType;
use App\Services\Paginator;
use App\Services\Fetcher;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    /**
     * @Route("/home", name="home")
     */
    public function index()
    {
        $em = $this->getDoctrine()->getRepository(Post::class)->findAll();
        return $this->render('home/index.html.twig', array('posts' => $em));
    }

    /**
     * @Route("/newpost", name="newpost")
     *
     *
     */
    public function newpost(Request $request)
    {
        $post = new Post();
//        $post->setTitle('oversea');
//        $post->setDescription('description');
        $form = $this->createForm(PostType::class, $post);

       $form->handleRequest($request);
       $image = 'ec87bfeb60d47472718d573ef7cfdace.png';
       if ($form->isSubmitted())
       {
           //get info file has upload, get by name
           $file = $request->files->get('post')['my_file'];
            //set parameter in services.yaml, then get directory
           $uploads_directory = $this->getParameter('uploads_directory');

           //md5 file name, guessextenstion to get extenstion file
           $filename = md5(uniqid()). '.' . $file->guessExtension();

           $file->move(
               $uploads_directory,
               $filename
           );

           echo '<pre>';
           var_dump($file); die;
            $em = $this->getDoctrine()->getManager();
            $em->persist($post);
            $em->flush();
       }

        return $this->render('home/greet.html.twig', [
            'form' => $form->createView(),
            'image' => $image
        ]);
    }

    /**
     * @Route("/uploads", name="newpost")
     *
     *
     */
    public function uploads(Request $request , Fetcher $fetcher, Paginator $paginator)
    {

        $post = new Post();
//        $post->setTitle('oversea');
//        $post->setDescription('description');
        $form = $this->createForm(PostType::class, $post);

        $form->handleRequest($request);
        $image = 'ec87bfeb60d47472718d573ef7cfdace.png';
        if ($form->isSubmitted())
        {
            //set parameter in services.yaml, then get directory
            $uploads_directory = $this->getParameter('uploads_directory');

            //get info file has upload, get by name
            $files = $request->files->get('post')['my_files'];

            foreach ($files as $file)
            {
                //md5 file name, guessextenstion to get extenstion file
                $filename = md5(uniqid()). '.' . $file->guessExtension();

                $file->move(
                    $uploads_directory,
                    $filename
                );
            }

            $em = $this->getDoctrine()->getManager();
            $em->persist($post);
            $em->flush();
        }

        $result = $fetcher->get('https://api.coinmarketcap.com/v2/listings');
        $partialArr = $paginator->getPartial($result['data'], 10, 20);

        return $this->render('home/greet.html.twig', [
            'form' => $form->createView(),
            'image' => $image,
            'partialArr' => $partialArr

        ]);
    }

    /**
     * @Route("/showpost/{id}", name="show_post")
     */
    public function showpost(Request $request, Post $post)
    {
        return $this->render('home/showpost.html.twig', [
            'post' => $post
        ]);
    }

}
