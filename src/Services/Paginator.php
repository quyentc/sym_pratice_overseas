<?php
namespace App\Services;
class Paginator{

    public function getPartial($data, $offset, $length)
    {
        return array_splice($data, $offset, $length);
    }
}